<?php namespace StudioBosco\PagePreview;

use App;
use Block;
use Event;
use Session;
use System\Classes\PluginBase;
use StudioBosco\PagePreview\Classes\Helper;
use StudioBosco\PagePreview\Classes\PreviewManager;
use StudioBosco\PagePreview\Models\Settings;

/**
 * PagePreview Plugin Information File
 */
class Plugin extends PluginBase
{
    use \System\Traits\ViewMaker;

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'studiobosco.pagepreview::lang.plugin.name',
            'description' => 'studiobosco.pagepreview::lang.plugin.description',
            'author'      => 'Studio Bosco <ondrej@studiobosco.de>',
            'icon'        => 'icon-eye',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        $self = $this;
        $isEnabled = Settings::get('use_for_static_pages', false) || Settings::get('use_for_mail_templates', false) || Settings::get('use_for_blog_posts', false);

        if (App::runningInBackend()) {
            if ($isEnabled) {
                $this->addViewPath(plugins_path() . '/studiobosco/pagepreview/partials');

                Event::listen(
                    'backend.page.beforeDisplay',
                    function () use ($self) {
                        Block::append('body', $self->makePartial('js_globals'));
                    }
                );
            }

            if (Helper::hasStaticPagesPlugin() && Settings::get('use_for_static_pages', false)) {
                Helper::extendStaticPagesController(function ($controller) {
                    $controller->addJs('/plugins/studiobosco/pagepreview/assets/js/debounce.js');
                    $controller->addJs('/plugins/studiobosco/pagepreview/assets/js/querySelectorParent.js');
                    $controller->addJs('/plugins/studiobosco/pagepreview/assets/js/shared.js');
                    $controller->addJs('/plugins/studiobosco/pagepreview/assets/js/staticpagepreview.js');
                    $controller->addCss('/plugins/studiobosco/pagepreview/assets/css/staticpagepreview.css');

                    $controller->addDynamicMethod('onPreviewUpdate', function () {
                        $data = post();
                        Session::put('studiobosco.pagepreview.staticpage_preview_data', $data);
                    });
                });

                // remove build-in preview field
                Event::listen('backend.form.extendFields', function($formWidget) {
                    if (
                        $formWidget->isNested
                        || !($formWidget->model instanceof \Winter\Pages\Classes\Page)
                    ) {
                        return;
                    }

                    $formWidget->removeField('_preview');
                });
            };

            if (Settings::get('use_for_mail_templates', false)) {
                PreviewManager::registerPreview(
                    '\System\Controllers\MailTemplates',
                    '\System\Models\MailTemplate'
                );

                \System\Models\MailTemplate::extend(function ($model) {
                    $model->addDynamicMethod('preview', function () use ($model) {
                        return \System\Classes\MailManager::instance()->renderTemplate($model);
                    });
                });
            }

            if (Settings::get('use_for_blog_posts') && Helper::hasBlogPlugin()) {
                PreviewManager::registerPreview(
                    '\Winter\Blog\Controllers\Posts',
                    '\Winter\Blog\Models\Post'
                );

                \Winter\Blog\Models\Post::extend(function ($model) {
                    $model->addDynamicMethod('preview', function () use ($model) {
                        return $model->formatHtml($model->content, true);
                    });
                });
            }

            foreach(PreviewManager::getPreviewableModelControllers() as [$controllerClass, $modelClass]) {
                $controllerClass::extend(function ($controller) use ($modelClass) {
                    $modelName = array_last(explode('\\', $modelClass));

                    $controller->addJs('/plugins/studiobosco/pagepreview/assets/js/debounce.js');
                    $controller->addJs('/plugins/studiobosco/pagepreview/assets/js/querySelectorParent.js');
                    $controller->addJs('/plugins/studiobosco/pagepreview/assets/js/shared.js');
                    $controller->addJs('/plugins/studiobosco/pagepreview/assets/js/modelpreview.js');
                    $controller->addCss('/plugins/studiobosco/pagepreview/assets/css/staticpagepreview.css');

                    $controller->addDynamicMethod('onPreviewUpdate', function () use ($modelName) {
                        $data = post();
                        Session::put('studiobosco.pagepreview.model_preview_data', array_get($data, $modelName, []));
                    });
                });

                $controllerClass::extendFormFields(function ($form, $model, $context) {
                    if ($context === 'update') {
                        if (!isset($form->allFields['preview_button'])) {
                            $form->addFields([
                                'preview_button' => [
                                    'type' => 'partial',
                                    'path' => '$/studiobosco/pagepreview/partials/_model_preview_button.htm',
                                ],
                            ]);
                        }
                    }
                });
            }
        } else {
            if (Helper::hasStaticPagesPlugin() && Settings::get('use_for_static_pages', false)) {
                Helper::extendStaticPage(function ($model) {
                    $model->bindEvent('model.afterFetch', function () use ($model) {
                        Helper::previewStaticPage($model);
                    });
                });
            }
        }
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'studiobosco.pagepreview.manage_settings' => [
                'tab'   => 'studiobosco.pagepreview::lang.plugin.name',
                'label' => 'studiobosco.pagepreview::lang.permissions.manage_settings'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [];
    }

    public function registerSettings()
    {
        return [
            'pagepreview' => [
                'label'       => 'studiobosco.pagepreview::lang.settings.label',
                'description' => 'studiobosco.pagepreview::lang.settings.description',
                'icon'        => 'icon-eye',
                'class'       => 'StudioBosco\PagePreview\Models\Settings',
                'order'       => 100,
                'permissions' => ['studiobosco.pagepreview.manage_settings']
            ],
        ];
    }
}
