# Page Preview Plugin

WinterCMS Plugin that replaces the "Preview" button for static pages, mail templates and other model forms with a real time preview.

After installing go to Settings > Page Preview and enable what you need.
Then click the "Preview" button in the edit form.

## Adding preview for model update forms

To create a preview button for a model you must first register the model form controller and the model class:

```php
\StudioBosco\PagePreview\Classes\PreviewManager::registerPreview($controllerClass, $modelClass);
```

Then you must add a "preview" method to the model:

```php
public function preview()
{
    return 'this is the preview';
}
```

Optionally you can add the preview button to the model fields:

```yaml
fields:
    preview_button:
        type: partial
        path: $/studiobosco/pagepreview/partials/_model_preview_button.htm
```

If you do not add it the plugin will add it automatically, but eventually in the wrong position.
