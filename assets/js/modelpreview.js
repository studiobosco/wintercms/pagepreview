(() => {
  let template = '';
  let preview = null;
  let isResizing = false;
  let isInited = false;
  let activePreviewUrl = null;

  function clamp(n, lower, upper) {
    if (n < lower) {
      return lower;
    }

    if (n > upper) {
      return upper;
    }

    return n;
  }

  function togglePreview(event) {
    event.preventDefault();
    event.stopPropagation();
    const preview = document.getElementById('static-page-preview');
    const isVisible = preview && !preview.classList.contains('is-hidden');

    if (isVisible) {
      closePreview();
    } else {
      const url = event.target.getAttribute('href');
      const form = querySelectorParent(event.target, 'form');
      openPreview(url, form);
    }

    return false;
  }

  function closePreview() {
    activePreviewUrl = null;
    const preview = document.getElementById('static-page-preview');

    if (preview) {
      preview.classList.add('is-hidden');
      document.body.classList.remove('has-visible-static-page-preview');
    }
  }

  function openPreview(url, form) {
    activePreviewUrl = url;
    document.body.classList.add('has-static-page-preview');
    const preview = getPreview(form, url);
    updatePreview(url, form, preview);
    preview.classList.remove('is-hidden');
    document.body.classList.add('has-static-page-preview');
    document.body.classList.add('has-visible-static-page-preview');
  }

  function getPreview(form, url) {
    if (!preview) {
      preview = document.createElement('div');
      preview.innerHTML = template;
      preview.setAttribute('id', 'static-page-preview');
      preview.classList.add('static-page-preview', 'is-hidden');
      document.getElementById('layout-body').appendChild(preview);
      const stickyHeader = document.querySelector('.layout-topmenu.sticky');

      if (stickyHeader) {
        preview.style.height = `calc(100vh - ${stickyHeader.offsetHeight}px)`;
        preview.style.top = `${stickyHeader.offsetHeight}px`;
      }

      const refreshPreviewButton = preview.querySelector('button[data-control="refresh-preview-button"]');
      const closePreviewButton = preview.querySelector('button[data-control="close-preview-button"]');
      const viewPageButton = preview.querySelector('a[data-control="view-page-button"]');
      const resizeHandle = preview.querySelector('*[data-control="resize-page-preview-handle"]');

      closePreviewButton.addEventListener('click', closePreview);
      refreshPreviewButton.addEventListener('click', function (event) {
        event.stopPropagation();
        event.preventDefault();
        updatePreview(url, form, preview);

        return false;
      });

      resizeHandle.addEventListener('mousedown', handleResizeHandleDragStart);
      resizeHandle.addEventListener('touchstart', handleResizeHandleDragStart);

      viewPageButton.setAttribute('href', url);
    }

    return preview;
  }

  function handleResizeHandleDragStart(event) {
    if (isResizing) {
      return;
    }

    isResizing = true;

    document.body.addEventListener('mousemove', handleResizeHandleDragMove);
    document.body.addEventListener('touchmove', handleResizeHandleDragMove);
    document.body.addEventListener('mouseup', handleResizeHandleDragEnd);
    document.body.addEventListener('touchend', handleResizeHandleDragEnd);
    document.body.classList.add('select-none', 'has-resizing-static-page-preview');
  }

  function handleResizeHandleDragMove(event) {
    if (!isResizing) {
      return;
    }

    const wrapper = document.getElementById('layout-body');
    const wrapperBound = wrapper.getBoundingClientRect();
    const resizeHandle = preview.querySelector('*[data-control="resize-page-preview-handle"]');

    const x = event.changedTouches ? event.changedTouches[0].clientX : event.clientX;

    const value = 1 - clamp(
      (x - wrapperBound.x) / wrapperBound.width,
      0,
      (wrapperBound.width - resizeHandle.getBoundingClientRect().width) / wrapperBound.width,
    );

    document.body.style.setProperty('--page-preview-width', (value * 100) + '%');
  }

  function handleResizeHandleDragEnd(event) {
    if (!isResizing) {
      return;
    }

    isResizing = false;
    document.body.removeEventListener('mousemove', handleResizeHandleDragMove);
    document.body.removeEventListener('touchmove', handleResizeHandleDragMove);
    document.body.removeEventListener('mouseup', handleResizeHandleDragEnd);
    document.body.removeEventListener('touchend', handleResizeHandleDragEnd);
    document.body.classList.remove('select-none', 'has-resizing-static-page-preview');
  }

  function handleFormInput(event) {
    const form = querySelectorParent(event.target, 'form');
    const previewButton = form.querySelector('a[data-control="preview-button"]');
    const url = previewButton.getAttribute('href');
    const preview = getPreview(form, url);
    if (preview.classList.contains('is-hidden')) {
        return;
    }
    updatePreview(url, form, preview);
  }

  function handleFormPointer(event) {
    // a drag occured, or a language switch occured
    // wait for it to finish and then trigger an update
    if (
      event.target.classList.contains('repeater-item-handle') ||
      event.target.classList.contains('placeholder') ||
      event.target.getAttribute('data-switch-locale')
    ) {
      const form = querySelectorParent(event.target, 'form');
      const previewButton = form.querySelector('a[data-control="preview-button"]');
      const url = previewButton.getAttribute('href');
      const preview = getPreview(form, url);
      if (preview.classList.contains('is-hidden')) {
        return;
      }

      setTimeout(() => {
        updatePreview(url, form, preview);
      }, 100);
    }
  }

  const updatePreview = debounce(function(url, form, preview) {
    const loadingIndicator = preview.querySelector('*[data-control="refresh-preview-loading-indicator"]');
    loadingIndicator.classList.add('active');

    $(form).request('onPreviewUpdate', {
      success: () => {
        const iframe = preview.querySelector('iframe[data-control="page-preview-iframe"]');
        const scrollY = iframe.contentWindow.scrollY;
        const scrollX = iframe.contentWindow.scrollX;

        // keep scroll position after update
        iframe.onload = function () {
          loadingIndicator.classList.remove('active');
          iframe.contentWindow.scrollTo(scrollX, scrollY);
          injectInlineEditing(form, iframe.contentDocument, iframe.contentWindow);
        };

        // force reload the iframe with a timestamp
        iframe.setAttribute('src', url + '?_preview=' + (new Date()).getTime());
      },
    });
  }, 500);

  function initModelPreview() {
    fetch(window.backendUrl + '/studiobosco/pagepreview/api/static-page-preview-template', {
      credentials: 'include',
    })
    .then((res) => {
      if (res.ok) {
        return res.text();
      } else {
        return res.text()
        .then((msg) => {
          throw new Error(msg);
        });
      }
    })
    .then((html) => {
      template = html;
      isInited = true;
      updateModelPreview();
    }, (err) => {
      console.error(err);
    });
  }

  function updateModelPreview() {
    const form = document.getElementById('Form');
    if (!form) {
        return;
    }

    // blog posts may already have a preview button
    const previewButtons = Array.from(form.querySelectorAll('a[data-control="preview-button"]'));
    const previewButton = previewButtons.pop();

    if (!previewButton) {
      return;
    }

    previewButton.removeEventListener('click', togglePreview);
    previewButton.addEventListener('click', togglePreview);
    const url = previewButton.getAttribute('href');

    // handle jQuery select updates
    $(form).off('select2:select', handleFormInput);
    $(form).on('select2:select', handleFormInput);

    // update on change
    form.removeEventListener('change', handleFormInput);
    form.addEventListener('mouseup', handleFormPointer);
    form.removeEventListener('touchend', handleFormPointer);
    form.addEventListener('change', handleFormInput);
    form.removeEventListener('mouseup', handleFormPointer);
    form.addEventListener('touchend', handleFormPointer);
  }

  $(document).render(function () {
    if (!isInited) {
      initModelPreview();
    } else {
      updateModelPreview();
    }
  });
})();
