function querySelectorParent(el, selector) {
  if (!el.parentElement || !el.parentElement.parentElement) {
    return null;
  }
  const match = Array.from(el.parentElement.parentElement.querySelectorAll(selector))
  .filter((parent) => {
    return parent == el.parentElement;
  })[0] || null;
  if (!match) {
    return querySelectorParent(el.parentElement, selector);
  } else {
    return match;
  }
}
