function injectInlineEditing(form, doc, win) {
  const pluginUrl = '/plugins/studiobosco/pagepreview'; // TODO: generate this using php
  const stylesUrl = pluginUrl + '/assets/css/inline_edit.css';
  doc.head.innerHTML += `<link rel="stylesheet" type="text/css" href="${stylesUrl}">`;

  Array.from(doc.querySelectorAll('*[data-field]')).forEach(initInlineEdit);

  function getFullFieldPath(el) {
    let elWithField = el;
    let fields = [];

    while(elWithField) {
      if (elWithField) {
        fields.unshift(elWithField.getAttribute('data-field'));
        elWithField = querySelectorParent(elWithField, '*[data-field]');
      }
    }

    const fieldPath = fields.join('.');

    return fieldPath;
  }

  function findFormField(fieldPath) {
    const fieldParts = fieldPath.split('.');

    let formField;

    for(let fieldPart of fieldParts) {
      const fieldPartAsInt = parseInt(fieldPart, 10);

      if (!isNaN(fieldPartAsInt)) {
        const fieldId = (formField || form).id;
        formField = Array.from(
          document.querySelectorAll('#' + fieldId + ' > .field-repeater > .field-repeater-items > .field-repeater-item')
        )[fieldPartAsInt] || Array.from(
          document.querySelectorAll('#' + fieldId + ' > .field-multilingual-repeater > div > .field-repeater > .field-repeater-items > .field-repeater-item')
        )[fieldPartAsInt] || null;
      } else {
        formField = (formField || form).querySelector('*[data-field-name="' + fieldPart + '"]');
      }

      if (!formField) {
        break;
      }
    }

    return formField;
  }

  function highlightFormField(formField) {
    // is field within a tab? If so activate the tab first.
    const parentTabPane = querySelectorParent(formField, '.tab-pane');

    if (parentTabPane && !parentTabPane.classList.contains('active')) {
      const tabLink = document.querySelector('*[data-target="#' + parentTabPane.id + '"]');
      if (tabLink) {
        tabLink.click();
      }
    }

    // is field within a collapsed repeater item? If so we need to expand it and also eventually collapsed parent repeater items.
    let collapsedParent = querySelectorParent(formField, '.field-repeater-item.collapsed');;

    while(collapsedParent) {
      collapsedParent.classList.remove('collapsed');
      collapsedParent = querySelectorParent(collapsedParent, '.field-repeater-item.collapsed');
    }

    // now we can scroll the field into view
    formField.scrollIntoView({
      behavior: 'smooth',
    });
    formField.focus();
    formField.classList.add('preview-inline-editing');

    // if this is a repeater item, expand it
    if (formField.classList.contains('field-repeater-item')) {
      formField.classList.remove('collapsed');
    }

    // if there is a top menu presetn we eventually need to adjust the scroll position, to keep the field in view
    const topmenu = document.querySelector('.layout-topmenu');

    if (topmenu) {
      setTimeout(() => {
        const bbox = formField.getBoundingClientRect();

        if (bbox.top < topmenu.clientHeight) {
          window.scrollBy({
            top: -topmenu.clientHeight,
            behavior: 'smooth',
          });
        }
      }, 300);
    }

    setTimeout(() => {
      formField.classList.remove('preview-inline-editing');
    }, 1000);
  }

  function injectAddRepeaterItem(el, fieldPath) {
    const formField = findFormField(fieldPath);
    const addBtn = doc.createElement('button');

    if (!formField) return;

    const formFieldId = formField.id;
    const addItemEl = document.querySelector('#' + formFieldId + ' > .field-repeater > .field-repeater-items > .field-repeater-add-item > *[data-repeater-add]') || document.querySelector('#' + formFieldId + ' > .field-repeater > .field-repeater-items > .field-repeater-add-item > *[data-repeater-add-group]');

    if (!addItemEl) return;

    addBtn.classList.add('preview-inline-editable-add-item');
    addBtn.innerHTML = addItemEl.innerHTML;
    el.appendChild(addBtn);

    addBtn.addEventListener('click', function (event) {
      event.preventDefault();
      event.stopPropagation();

      highlightFormField(addItemEl);

      setTimeout(function () {
        addItemEl.click();
      }, 300);

      return false;
    });
    addBtn.addEventListener('mouseenter', function (event) {
      let overlay = doc.getElementById('preview-inline-editable-overlay');
      if (overlay) {
        overlay.remove();
      }
    });
  }

  function injectRemoveRepeaterItem(overlay, fieldPath) {
    const formField = findFormField(fieldPath);
    if (!formField) return;

    const removeItemEl = formField.querySelector('*[data-repeater-remove]');

    if (!removeItemEl) return;
    removeBtn = doc.createElement('button');
    removeBtn.classList.add('preview-inline-editable-remove-item');
    removeBtn.innerHTML = '<div>&times;</div>';
    overlay.appendChild(removeBtn);
    debugger
    removeBtn.addEventListener('click', function (event) {
      event.preventDefault();
      event.stopPropagation();
      overlay.appendChild(removeBtn);

      highlightFormField(removeItemEl);
      setTimeout(function () {
        removeItemEl.click();
      }, 300);

      return false;
    });
  }

  function initInlineEdit(el) {
    const fieldTye = el.getAttribute('data-field-type');
    const fieldPath = getFullFieldPath(el);

    if (fieldTye === 'repeater') {
      injectAddRepeaterItem(el, fieldPath);
    }

    el.addEventListener('mouseenter', function (event) {
      const bbox = el.getBoundingClientRect();
      let overlay = doc.getElementById('preview-inline-editable-overlay');
      if (!overlay) {
        overlay = doc.createElement('div');
        overlay.setAttribute('id', 'preview-inline-editable-overlay');
        doc.body.appendChild(overlay);
      }
      overlay.style.left = (bbox.left + win.scrollX) + 'px';
      overlay.style.top = (bbox.top + win.scrollY) + 'px';
      overlay.style.width = bbox.width + 'px';
      overlay.style.height = bbox.height + 'px';
      overlay.classList.add('visible');

      let removeBtn = overlay.querySelector('.preview-inline-editable-remove-item');

      if (removeBtn) {
        removeBtn.remove();
      }

      if (fieldTye === 'repeater-item') {
        injectRemoveRepeaterItem(overlay, fieldPath);
      }
    });

    el.addEventListener('mouseleave', function (event) {
      const overlay = doc.getElementById('preview-inline-editable-overlay');
      if (overlay) {
        overlay.remove();
      }
    });

    el.addEventListener('click', function (event) {
      event.preventDefault();
      event.stopPropagation();

      const formField = findFormField(fieldPath);

      if (formField) {
        highlightFormField(formField);
      }

      return false;
    });
  }
}
