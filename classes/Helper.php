<?php namespace StudioBosco\PagePreview\Classes;

use Input;
use Session;
use System\Classes\PluginManager;

class Helper
{
  public static function isOctoberPages()
  {
    return PluginManager::instance()->hasPlugin('Rainlab.Pages');
  }

  public static function isWinterPages()
  {
    return PluginManager::instance()->hasPlugin('Winter.Pages');
  }

  public static function hasStaticPagesPlugin()
  {
    return PluginManager::instance()->hasPlugin('Rainlab.Pages') || PluginManager::instance()->hasPlugin('Winter.Pages');
  }

  public static function hasBlogPlugin()
  {
      return PluginManager::instance()->hasPlugin('Winter.Blog');
  }

  public static function isStaticPageInstance($model)
  {
    return self::isWinterPages()
      ? $model instanceof \Winter\Pages\Classes\Page
      : $model instanceof \RainLab\Pages\Classes\Page;
  }

  public static function isStaticPagesController($controller)
  {
    return self::isWinterPages()
      ? $controller instanceof \Winter\Pages\Controllers\Index
      : $controller instanceof \RainLab\Pages\Controllers\Index;
  }

  public static function extendStaticPagesController($callback)
  {
    return self::isWinterPages()
      ? \Winter\Pages\Controllers\Index::extend($callback)
      : \RainLab\Pages\Controllers\Index::extend($callback);
  }

  public static function extendStaticPage($callback)
  {
    return self::isWinterPages()
      ? \Winter\Pages\Classes\Page::extend($callback)
      : \RainLab\Pages\Classes\Page::extend($callback);
  }

  public static function previewStaticPage($page)
  {
    if (!Input::has('_preview') || !intval(Input::get('_preview'))) {
      return;
    }

    $previewData = Session::get('studiobosco.pagepreview.staticpage_preview_data');
    if (!$previewData || !isset($previewData['viewBag'])) {
      return;
    }

    if ($previewData['objectPath'] == $page->getBaseFileName()) {
      $page->fill(['settings' => ['viewBag' => $previewData['viewBag']]]);
    }
  }
}
