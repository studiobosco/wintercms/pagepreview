<?php namespace StudioBosco\PagePreview\Classes;

class PreviewManager
{
    protected static $previewableModelControllers = [];

    public static function registerPreview(string $controllerClass, string $modelClass)
    {
        self::$previewableModelControllers[] = [$controllerClass, $modelClass];
    }

    public static function getPreviewableModelControllers()
    {
        return self::$previewableModelControllers;
    }
}
