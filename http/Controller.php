<?php namespace StudioBosco\PagePreview\Http;

use Session;
use Input;

use System\Models\MailTemplate;
use System\Classes\MailManager;
use StudioBosco\PagePreview\Classes\Helper;
class Controller extends \Backend\Classes\Controller
{
    use \Backend\Traits\FormModelSaver;

    public function showStaticPagePreviewTemplate()
    {
        $this->addViewPath(plugins_path() . '/studiobosco/pagepreview/partials');
        return $this->makePartial('preview_template');
    }

    public function modelPreview($modelClass, $id)
    {
        $this->addViewPath(plugins_path() . '/studiobosco/pagepreview/partials');
        $model = $modelClass::find($id);

        if (!$model) return false;

        if (Input::has('_preview') && intval(Input::get('_preview'))) {
            $previewData = Session::get('studiobosco.pagepreview.model_preview_data');

            if ($previewData && is_array($previewData)) {
                $this->setModelAttributes($model, $previewData);
            }
        }

        return $this->makePartial('model_preview', [
            'model' => $model,
        ]);
    }
}
