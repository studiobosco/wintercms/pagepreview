<?php return [
  'plugin' => [
    'name' => 'Seitenvorschau',
    'description' => 'Ersetzt den "Vorschau" Button bei statischen Seiten mit einer Echtzeitvorschau der Seite.',
  ],
  'preview' => 'Vorschau',
  'refresh_preview_label' => 'Aktualisieren',
  'refresh_preview_title' => 'Vorschau aktualisieren',
  'close_preview_title' => 'Vorschau schließen',
  'view_page_label' => 'Ansehen',
  'view_page_title' => 'Seite in neuem Tab öffnen',
  'resize_preview_handle_label' => 'Zieheh, um die Größe der Vorschau zu ändern.',

  'settings' => [
    'label' => 'Seitenvorschau',
    'description' => 'Einstellungen für die Seitenvorschau',
    'use_for_static_pages' => 'Für Seiten aktivieren',
    'use_for_mail_templates' => 'Für Mail-Vorlagen aktivieren',
    'use_for_blog_posts' => 'Für Blog-Artikel aktivieren',
    'blog_post_page' => 'Seite mit Blog-Artikel-Komponente',
  ],

  'permissions' => [
    'manage_settings' => 'Einstellungen verwalten',
  ],
];
