<?php
namespace StudioBosco\PagePreview\Models;

use Model;
use Cms\Classes\Page;
use Cms\Classes\Theme;

class Settings extends Model{

    public $implement = [
        'System.Behaviors.SettingsModel'
    ];

    public $settingsCode = 'studiobosco_pagepreview_settings';

    public $settingsFields = 'fields.yaml';

    public function getBlogPostPageOptions()
    {
        $options = [];
        $theme = Theme::getActiveTheme();

        foreach(Page::listInTheme($theme) as $page) {
            $options[str_replace('.htm', '', $page->fileName)] = $page->title;
        }

        return $options;
    }
}
