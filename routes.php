<?php

Route::group(['prefix' => Config::get('cms.backendUri'), 'middleware' => ['web']], function ($router) {
    $router->group(['prefix' => 'studiobosco/pagepreview/api'], function ($router) {
        $router->get('static-page-preview-template', 'StudioBosco\PagePreview\Http\Controller@showStaticPagePreviewTemplate');
        $router->get('model-preview/{modelClass}/{id}', 'StudioBosco\PagePreview\Http\Controller@modelPreview');
    });
});
