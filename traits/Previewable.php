<?php namespace StudioBosco\PagePreview\Traits;

use File;
use Cms\Classes\Controller as CmsController;

trait Previewable
{
    protected $twig;

    public function preview()
    {
        return $this->renderPartial('preview.htm', [
            '__SELF__' => $this,
            'model' => $this,
        ]);
    }

    protected function initTwig()
    {
        if (!$this->twig) {
            $viewPath = $this->viewPath ?? $this->guessViewPath('partials');
            $controller = new CmsController();
            $this->twig = $controller->getTwig();
            $loader = new \Twig\Loader\FilesystemLoader($viewPath);
            $this->twig->setLoader($loader);
        }
    }

    protected function guessViewPath($suffix)
    {
        $suffix = DIRECTORY_SEPARATOR . $suffix;
        $class = get_called_class();
        $classFolder = strtolower(class_basename($class));
        $classFile = realpath(dirname(File::fromClass($class)));
        $guessedPath = $classFile ? $classFile . DIRECTORY_SEPARATOR . $classFolder . $suffix : null;
        return $guessedPath;
    }

    public function renderPartial($path, $vars = [])
    {
        $this->initTwig();

        return $this->twig->render($path, $vars);
    }
}
